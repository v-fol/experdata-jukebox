import React, { useState } from "react";
import FirstTrackImage from "../images/image-1.jpg";
import SecondTrackImage from "../images/image-2.jpg";
import ThirdTrackImage from "../images/image-3.jpeg";
import FourthTrackImage from "../images/image-4.jpg";
import FirstTrackCover from "../images/cover-1.jpg";
import SecondTrackCover from "../images/cover-2.jpg";
import ThirdTrackCover from "../images/cover-3.png";
import FourthTrackCover from "../images/cover-4.jpg";
import { PlayIcon, PauseIcon } from '@heroicons/react/solid'

import Track from "../components/Track.js"
import "../App.css"


const songs = [
{
    'composer': 'Herrick',
    'composition': 'Cherry Ripe',
    'url': 'https://www.mfiles.co.uk/mp3-downloads/cherry-ripe.mp3',
    'image': FirstTrackImage,
    'cover': FirstTrackCover,
    'position': 'first'
},
{
    'composer': 'Joplin',
    'composition': 'Peacherine Rag',
    'url': 'https://www.mfiles.co.uk/mp3-downloads/scott-joplin-peacherine-rag.mp3',
    'image': SecondTrackImage,
    'cover': SecondTrackCover,
    'position': 'middle'
},
{
    'composer': 'Filtsch',
    'composition': 'Romance',
    'url': 'https://www.mfiles.co.uk/mp3-downloads/carl-filtsch-romance-op3-no1.mp3',
    'image': ThirdTrackImage,
    'cover': ThirdTrackCover,
    'position': 'middle'
},
{
    'composer': 'Tchaikovsky',
    'composition': 'Waltz',
    'url': 'https://www.mfiles.co.uk/mp3-downloads/Tchaikovsky-Waltz-op39-no8.mp3',
    'image': FourthTrackImage,
    'cover': FourthTrackCover,
    'position': 'last'
},
]


function Jukebox() {

    const[songComposer, setSongComposer] = useState(songs[0].composer)
    const[songComposition, setSongComposition] = useState(songs[0].composition)
    const[songImage, setSongImage] = useState(songs[0].image)
    const[songCover, setSongCover] = useState(songs[0].cover)
    const[songUrl, setSongUrl] = useState(songs[0].url)
    const[playPause, setPlayPause] = useState(false)

    return (
        <div className="bg-gray-900 select-none">
            <div className="min-w-screen min-h-screen flex">
                <div className="container m-auto w-7/12 grid grid-cols-3" style={{ backgroundImage:`url(${songCover})` }}>

                    <div className="w-96 z-50">
                        {songs.map((song, index)=> (
                        <div key={index} className={ songComposition === song.composition ? `h-44 bg-red-300 w-100 rounded-r-md ${song.position} p-14 block transition duration-300 ease-in-out filter drop-shadow-lg` : `transition duration-500 ease-in-out h-44 bg-gray-100 w-96 ${song.position} p-14 block hover:bg-red-100 `}>
                            
                            <img className="h-16 w-16 rounded-full object-cover inline" src={song.image} alt=""></img>
                            <div className="inline-block ml-4">
                                <h4 className="-mt-7 absolute text-xl uppercase font-style-bold">{song.composer}</h4>
                                <h5 className="-mt-1 absolute text-xl font-style-light">{song.composition}</h5>
                            </div>
                            <div className="inline-block ">
                                { songComposition === song.composition && playPause === true ? <PauseIcon className="ml-56 h-16 -mt-20 cursor-pointer" onClick={() => {setPlayPause(false)}} />  :  <PlayIcon className="ml-56 h-16 -mt-20 cursor-pointer"  onClick={() => { setPlayPause(true); setSongComposer(song.composer); setSongComposition(song.composition); setSongImage(song.image); setSongCover(song.cover); setSongUrl(song.url)}} />}
                            </div>

                        </div>
                        ))}
                    </div>

                    <div className="w-full h-full backdrop-filter grid grid-cols-1 backdrop-blur-sm z-10 col-span-2 blur-container px-4 py-4">
                        <h1 className="font-style-bold text-3xl text-center text-white">Experdata Jukebox</h1>
                        <img className="w-56 h-52 object-cover rounded-3xl place-self-center mt-32 filter drop-shadow-2xl" src={songImage} alt=""></img>
                        <div className="w-48 h-20 object-cover rounded-3xl place-self-center -mt-20 bg-red-400 filter drop-shadow-2xl p-2">
                            <h3 className="text-center align-middle text-xl uppercase font-style-bold">{songComposer}</h3>
                            <h4 className="text-center align-middle text-xl font-style-light">{songComposition}</h4>
                        </div>
                        <Track playPause={playPause} url={songUrl} />
                   </div>
 
                </div>
            </div>
        </div>
    )
}

export default Jukebox
