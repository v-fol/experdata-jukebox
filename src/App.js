import './App.css';
import Jukebox from './pages/Jukebox.js'

function App() {
  return (
    <div className="App">
      <Jukebox/>
    </div>
  );
}

export default App;
