import React, { Component } from 'react';
import WaveSurfer from 'wavesurfer.js';


class Track extends Component { 
   
  state = {
    playing: false,
    url: 'https://www.mfiles.co.uk/mp3-downloads/cherry-ripe.mp3',
  };

  componentDidMount() {
    const track = document.querySelector('#track');

    this.waveform = WaveSurfer.create({
      barWidth: 5,
      cursorWidth: 1,
      container: '#waveform',
      backend: 'WebAudio',
      height: 140,
      progressColor: '#e6726a',
      responsive: true,
      waveColor: '#FFFFFF',
      cursorColor: 'transparent',
    });

    this.waveform.load(track);
  };

  componentDidUpdate() {

    // This statement is used to prevent recursion. 
    // It checks if the current state is equal to the state that is passed through the prop, and if the prop is different its changes the state.
    if (this.props.url !== this.state.url) {
      this.setState({ url: this.props.url});
      this.waveform.load(this.props.url)
    }
    // This statement is used to prevent recursion. 
    if (!this.props.playPause === this.state.playing){
      this.setState({ playing: this.props.playPause });
      this.waveform.playPause();
    }
    // Plays the song after it's been loaded.
    this.waveform.on('ready', () =>{
      this.waveform.play();
    });
    
  }

  render() {

    return (
      <div>
        <div id="waveform" />
        <audio id="track" src={this.state.url} />
      </div>
    );
  }
};

export default Track;