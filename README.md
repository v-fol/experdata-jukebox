# Experdata Jukebox (task - 3)

![Draft](https://i.ibb.co/PjWSrgy/Experdata-Jukebox.png)
[Figma design](https://www.figma.com/file/90feWar30qG9V7Hv9yfE5v/Experdata-Jukebox?node-id=0%3A1)

## Available Scripts

In the project directory, you can run:

### `npm install` to install packages
### `npm run start` to open dev server at http://localhost:3000/

